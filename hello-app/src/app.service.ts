import { Injectable } from '@nestjs/common';
import { Greeting } from './model/greeting.dto';

@Injectable()
export class AppService {

  getHello(): Greeting {
    return { message: 'Hello World!'};
  }

  getPersonalizedHello(name: string): Greeting {
    return { message: `Hello ${name}!`};
  }

}
