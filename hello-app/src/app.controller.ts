import { Controller, Get, Param } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { AppService } from './app.service';
import { Greeting } from './model/greeting.dto';

@ApiTags('greetings')
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @ApiResponse({ status: 200, description: "Successfully generated the default greeting." })
  @Get()
  getHello(): Greeting {
    return this.appService.getHello();
  }

  @Get(':name')
  @ApiResponse({ status: 200, description: "Successfully generated a personalized greeting." })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  getPersonalizedHello(@Param('name') name: string): Greeting {
    return this.appService.getPersonalizedHello(name);
  }
}


